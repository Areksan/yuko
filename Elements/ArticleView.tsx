import React, {useState} from "react";
import {Button, Modal, Text} from "react-native";
import {ArticleInterface} from "../Pages/ArticleList";

export default function ArticleView(props) {
    let article: ArticleInterface = props.article;
    if (article === null || props.modalVisible === false) {
        return null;
    }
    return (
        <Modal
            animationType="slide"
            transparent={false}
            visible={props.modalVisible}>
            <Text>{article.name}</Text>
            <Text>{article.heureScan}</Text>
            <Button title={"close"} onPress={() => {
                props.setModalVisible(false);
                props.setCurrentArticle(null);
            }}/>
        </Modal>
    )
}