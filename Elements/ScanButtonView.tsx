import React from "react";
import {Button, StyleSheet, View} from "react-native";

export default function ScanButtonView(){
    return(
        <View style={styles.scan}>
            <Button title={"SCAN"} onPress={() => {
                console.log("SCAN");
            }}/>
        </View>
    );
}

const styles = StyleSheet.create({
    scan: {
        padding: 20,
    }
})