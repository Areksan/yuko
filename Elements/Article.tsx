import React from "react";
import {StyleSheet, Text, View} from "react-native";
import {ArticleInterface} from "../Pages/ArticleList";

export default function Article(props:ArticleInterface) {
    return(
        <View style={styles.article}>
            <Text style={styles.articleName}>{props.name}</Text>
            <Text>Scan effectué à {props.heureScan}H</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    article: {
        height: 100,
        backgroundColor: '#5d5d5d',
        marginBottom: 5,
        justifyContent: "center",
        padding: 15,
        marginLeft: 15,
        marginRight: 15
    },
    articleName: {
        fontSize: 18,
    }
})