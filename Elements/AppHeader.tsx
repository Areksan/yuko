import React from "react";
import {Text, View,StyleSheet} from "react-native";

export default function AppHeader(){
    return(
        <View style={styles.header}>
            <Text style={styles.title}>Yoki</Text>
        </View>
    );
}

const styles = StyleSheet.create({
    header: {
        width: "auto",
        height: 100,
        alignItems: "center",
        justifyContent: "center",
        backgroundColor: '#1a1a1a',
    },
    title: {
        fontSize: 30,
        fontWeight: "bold",
        color: "#eee"
    }
})