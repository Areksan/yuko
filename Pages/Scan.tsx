import React, { useState, useEffect } from 'react';
import { Text, View, StyleSheet, Button } from 'react-native';
import { BarCodeScanner } from 'expo-barcode-scanner';
import {ArticleInterface} from "./ArticleList";

export default function Scan(props) {
    const [hasPermission, setHasPermission] = useState(null);
    const [scanned, setScanned] = useState(false);

    useEffect(() => {
        (async () => {
            const { status } = await BarCodeScanner.requestPermissionsAsync();
            setHasPermission(status === 'granted');
        })();
    }, []);

    function addArticle(article : ArticleInterface) {
        console.log(article);
        let articleAray = [...props.article,article];
        props.setArticle(articleAray);
        let key:number = props.cle +1 ;
        props.setKey(key);
    }

    const handleBarCodeScanned = async ({type, data}) => {
        console.log("start");
        setScanned(true);
        let informations = await getInformation(data);
        let brand = informations.brands;
        let productName = brand +" "+informations.ecoscore_data.agribalyse.name_fr;
        let date = new Date();
        addArticle({code: informations._id,key:props.cle,name:productName,heureScan:date.getHours()});
        alert(`Bar code with type ${type} and data ${productName} has been scanned at ${date.getHours()} : ${date.getMinutes()}!`);
    };

    async function getInformation(data:any) {
        try {
            let res = await fetch("https://fr.openfoodfacts.org/api/v0/product/"+data+".json");
            let resJson = await res.json();
            return resJson.product;
        } catch (e) {
            console.error(e);
        }
    }

    if (hasPermission === null) {
        return <Text>Requesting for camera permission</Text>;
    }
    if (hasPermission === false) {
        return <Text>No access to camera</Text>;
    }

    return (
        <View style={styles.container}>
            <BarCodeScanner
                onBarCodeScanned={scanned ? undefined : handleBarCodeScanned}
                style={StyleSheet.absoluteFillObject}
            />
            {scanned && <Button title={'Tap to Scan Again'} onPress={() => setScanned(false)} />}
        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
    },
});
