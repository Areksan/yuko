import React, {useEffect, useState} from 'react';
import {
    Button,
    FlatList,
    ScrollView,
    StyleSheet,
    Text,
    TouchableHighlight,
    TouchableHighlightComponent,
    View
} from 'react-native';
import Article from "../Elements/Article";
import ScanButtonView from "../Elements/ScanButtonView";
import ArticleView from "../Elements/ArticleView";

export interface ArticleInterface {
    key: number,
    name: string,
    heureScan: number,
    code: number,
}

export default function ArticleList(props) {
    let defaultArticleList: ArticleInterface[] = props.article;
    const [modalVisible, setModalVisible] = useState(false);
    const [currentArticle, setCurrentArticle] = useState(null);

    useEffect(() => {
        if (currentArticle != null) {
            setModalVisible(true);
        }
    }, [currentArticle])

    return (
        <View style={styles.container}>
            <ArticleView modalVisible={modalVisible} article={currentArticle} setModalVisible={setModalVisible}
                         setCurrentArticle={setCurrentArticle}/>
            <ScanButtonView/>
            <FlatList data={defaultArticleList} renderItem={
                ({item}) =>
                    <View>
                        <TouchableHighlight
                            activeOpacity={0.6}
                            onPress={() => {
                                setCurrentArticle(item);
                            }}
                        >
                            <Article name={item.name} heureScan={item.heureScan} key={item.key} code={item.code}/>
                        </TouchableHighlight>
                    </View>
            }/>
        </View>
    );
}

const styles = StyleSheet.create({
        container:
            {
                flex: 1,
                backgroundColor: '#1a1a1a',
            },
    }
);
