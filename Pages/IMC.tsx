import React, {useEffect, useState} from "react";
import {Picker, StyleSheet, Text, TextInput, View} from "react-native";

export default function IMC() {
    const [sexe, setSexe] = useState("Homme");
    const [taille, setTaille] = useState('0');
    const [poids, setPoids] = useState('0');
    const [imc, setImc] = useState(0);
    const [aff, setAff] = useState(false);
    const [type, setType] = useState('');

    useEffect(() => {
        calcImc();
        typeIMC();
        if (imc > 0 && imc < 400) {
            setAff(true)
        } else {
            setAff(false)
        }
    });

    function calcImc() {
        let tempoPoids: number = parseFloat(poids);
        let tempoTaille: number = parseFloat(taille) / 100;
        let tempoImc: number = (tempoPoids / (tempoTaille * tempoTaille));
        setImc(tempoImc);
    }

    function typeIMC() {
        if (imc <18.5){
            setType("Insufisance Pondérale");
        }
        if(imc >= 18.5 && imc < 25 ){
            setType("Corpulance Normal");
        }
        if(imc >= 25 && imc < 30){
            setType("Surpoids");
        }
        if(imc >= 30 && imc < 35){
            setType("Obésité Modérée");
        }
        if(imc >= 35 && imc < 40){
            setType("Obésité Sévére");
        }
        if(imc >= 40){
            setType("Obésité morbide ou massive");
        }
    }

    return (
        <View style={styles.container}>
            <Text>Votre sexe</Text>
            <Picker selectedValue={sexe} onValueChange={(value) => {
                setSexe(value)
            }}>
                <Picker.Item label={"Homme"} value={"Homme"}/>
                <Picker.Item label={"Femme"} value={"Femme"}/>
            </Picker>
            <Text>Votre taille</Text>
            <TextInput
                keyboardType={"numeric"}
                onChangeText={(value) => {
                    setTaille(value);
                }
                }
                value={taille}
            />
            <Text>Votre poids</Text>
            <TextInput
                keyboardType={"numeric"}
                onChangeText={(value) => {
                    setPoids(value);
                }}
                value={poids}
            />
            <Toast affichage={aff} typeImc={type} imc={imc} sexe={sexe}/>
        </View>
    );
}

function Toast(props: any) {
    let pronom:string = "";
    switch (props.sexe){
        case "Homme":
            pronom = "un";
            break;
        case "Femme":
            pronom = "une";
            break;
    }
    if (props.affichage) {
        return (
            <View>
                <Text>Vous êtes {pronom} {props.sexe} </Text>
                <Text>Valeur de votre Imc: {props.imc}</Text>
                <Text>C'est que du blabla</Text>
                <Text>Mais : {props.typeImc}</Text>
            </View>
        )
    } else {
        return (
            <View>
                <Text>Valeur non valide</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
        container: {
            flex: 1,
            backgroundColor: '#eee',
        },
    }
);
