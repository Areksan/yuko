import {StatusBar} from 'expo-status-bar';
import React, {useState} from 'react';
import {Platform, SafeAreaView, StyleSheet, View} from 'react-native';
import {Button} from "react-native-elements";
import ArticleList from "./Pages/ArticleList";
import AppHeader from "./Elements/AppHeader";
import IMC from "./Pages/IMC";
import Scan from "./Pages/Scan";

export default function App() {
    const [page, setPage] = useState("LIST");
    const [article, setArticle] = useState([]);
    const [key,setKey] = useState(1);

    return (
        <SafeAreaView style={styles.container}>
            <AppHeader/>
            {
                (page === "LIST") &&
                <ArticleList article={article}/>
            }
            {
                (page === "IMC") &&
                <IMC/>
            }
            {
                (page === 'SCAN') &&
                    <Scan setArticle={setArticle} cle={key} setKey={setKey} />
            }
            <View style={styles.menu}>
                <Button type="solid" title={"LIST"} onPress={ () => {setPage("LIST");console.log(article)}}/>
                <Button title={"IMC"} onPress={ () => setPage("IMC")}/>
                <Button title={"SCAN"} onPress={ () => setPage("SCAN")}/>
            </View>
            <StatusBar style="auto"/>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#1a1a1a',
        paddingTop: Platform.OS === 'android' ? 25 : 0
    },
    logo: {
        width: 128,
        height: 128,
    },
    menu: {
        flexDirection:"row",
        marginHorizontal:15,
    }
});
